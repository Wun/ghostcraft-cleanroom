package lc.hex.ghostcraft.effect;

import org.bukkit.Location;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Data
@EqualsAndHashCode
public class ParticleInstance {
    private final Location location;
    private final int r, g, b;
}
