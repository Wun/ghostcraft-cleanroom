package lc.hex.ghostcraft.effect;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.logging.Logger;

import lombok.Getter;

public class ParticleRenderWorker implements Runnable {
    private final ParticleAgent parent;
    @Getter
    private final int id;
    @Getter
    private volatile boolean running = true;
    private Logger logger;

    public ParticleRenderWorker(ParticleAgent parent, int id) {
        this.parent = parent;
        this.id = id;
        this.logger = Logger.getLogger("GC Particle Worker #" + id);
    }

    public void stop() {
        running = false;
    }

    @Override
    public void run() {
        logger.info("Starting particle worker thread #" + id);
        while(running) {
            if (parent.backingQueue && parent.renderQueue.isEmpty() && !parent.renderBackingQueue.isEmpty()) {
                long l = System.currentTimeMillis();
                logger.info("Render queue empty, starting backfill...");
                for (ParticleInstance p = parent.renderBackingQueue.remove(); !parent.renderBackingQueue.isEmpty(); p = parent.renderBackingQueue.remove()) {
                    parent.renderQueue.add(p);
                }
                long time = System.currentTimeMillis() - l;
                logger.info("Render queue backfill complete, finished in " + time + " milliseconds.");
                parent.backingQueue = false;
            }
            try {
                ParticleInstance particle = parent.renderQueue.take(); // Blocking call
                Location location = particle.getLocation();
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (player.getLocation().distanceSquared(location) <= ParticleAgent.MAX_VIEW_DISTANCE * ParticleAgent.MAX_VIEW_DISTANCE) {
                        parent.displayParticlesTo(particle.getR(), particle.getG(), particle.getB(), location, player);
                    }
                }
            } catch (InterruptedException e) {
                logger.warning("Interrupted while waiting for a particle.");
                continue;
            }
        }
        logger.info("Shutting down.");
    }
}
