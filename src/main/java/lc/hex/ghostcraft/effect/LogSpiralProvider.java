package lc.hex.ghostcraft.effect;

import org.bukkit.util.Vector;

import lombok.Getter;

/**
 * Computes a number series for a logarithmic spiral.
 */
public class LogSpiralProvider {
    @Getter
    private final int loops;
    @Getter
    private final double density;
    private final double maxTheta;
    private final double n1Coeff;
    @Getter
    private final double size;
    @Getter
    private double theta;
    @Getter
    private boolean finished = false;

    /**
     *
     * @param loops The number of times the spiral touches the X axis
     * @param density The number to increment by
     * @param size The distance from the origin to the target end point
     */
    public LogSpiralProvider(int loops, double density, double size) {
        this.loops = loops;
        this.density = density;
        this.size = size;
        this.maxTheta = loops * Math.PI;
        this.n1Coeff = 1 / maxTheta;
        this.theta = 0D;
    }

    // Equation is r = n1Coeff(size)(θ)
    public Vector next() {
        if (finished) {
            return null;
        }
        // We'll have to convert polar coordinates to a rectangular system
        double polarRadius = n1Coeff * size * theta;
        double x = Math.cos(theta) * polarRadius;
        double y = Math.sin(theta) * polarRadius;

        theta += density;

        if (theta >= maxTheta) {
            finished = true;
        }

        // y --> z when going to a vector
        return new Vector(x, 0D, y);
    }

    public void reset() {
        this.theta = 0D;
        this.finished = false;
    }
}
