package lc.hex.ghostcraft.effect;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

import lc.hex.ghostcraft.GhostcraftPlugin;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public final class ParticleAgent {
    private final GhostcraftPlugin plugin;
    public static final int MAX_VIEW_DISTANCE = 128;
    public static final int MAX_RENDER_QUEUE_SIZE = 8192;
    public static final int WORKER_THREADS = 2;
    private ParticleRenderWorker[] workers = new ParticleRenderWorker[WORKER_THREADS];

    // We use two different queues because the insertion/removal time for an array-backed
    // queue is much less than a linked queue, but an array queue has a fixed size. Thus,
    // have a pause whenever the render queue is empty and needs to be backfilled. Also,
    // note that if the render queue is full, the flag should be set and the backing queue
    // should be inserted into instead.
    ArrayBlockingQueue<ParticleInstance> renderQueue;
    ConcurrentLinkedQueue<ParticleInstance> renderBackingQueue;
    volatile boolean backingQueue = false;

    {
        this.renderBackingQueue = new ConcurrentLinkedQueue<>();
        this.renderQueue = new ArrayBlockingQueue<>(MAX_RENDER_QUEUE_SIZE);
        for (int i = 0; i < WORKER_THREADS; i++) {
            ParticleRenderWorker worker = new ParticleRenderWorker(this, i + 1);
            Bukkit.getScheduler().runTaskAsynchronously(GhostcraftPlugin.instance(), worker);
            workers[i] = worker;
        }
    }

    /**
     * Queue a particle for async display.
     * @param particle The particle to display.
     */
    public synchronized void queueParticle(ParticleInstance particle) {
        if (backingQueue) {
            renderBackingQueue.add(particle);
        } else {
            if (!renderQueue.offer(particle)) {
                Bukkit.broadcastMessage("Overfill!");
                backingQueue = true;
                renderBackingQueue.add(particle);
            }
        }
    }

    /**
     * Queues a set of particles for display. They will be successively added to the
     * display queue.
     * @param particles A set of particles to queue.
     */
    public synchronized void queueParticles(Collection<ParticleInstance> particles) {
        particles.forEach(this::queueParticle);
    }

    /**
     * Queues a single particle to appear for a certain length of time
     * @param particle The particle to render
     * @param duration The duration the particle is to exist for (in ticks)
     */
    public synchronized void queueParticleRepeating(ParticleInstance particle, long duration) {
        new BukkitRunnable() {
            long time = duration;
            @Override
            public void run() {
                queueParticle(particle);
                time--;
                if (time <= 0) {
                    this.cancel();
                }
            }
        }.runTaskTimerAsynchronously(plugin, 1L, 1L);
    }

    public synchronized void queueParticlesRepeating(Collection<ParticleInstance> particles, long duration) {
        new BukkitRunnable() {
            long time = duration;
            @Override
            public void run() {
                queueParticles(particles);
                time--;
                if (time <= 0) {
                    this.cancel();
                }
            }
        }.runTaskTimerAsynchronously(plugin, 1L, 1);

    }

    public void displayParticlesTo(float r, float g, float b, Location l, Player p) {
        net.minecraft.server.v1_11_R1.PacketPlayOutWorldParticles packet = new net.minecraft.server.v1_11_R1.PacketPlayOutWorldParticles(
                net.minecraft.server.v1_11_R1.EnumParticle.REDSTONE, true,
                (float) l.getX(), (float) l.getY(), (float) l.getZ(),
                r, g, b,
                1, 0);
        ((org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
    }
}
