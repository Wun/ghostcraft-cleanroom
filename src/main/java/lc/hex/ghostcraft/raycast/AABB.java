package lc.hex.ghostcraft.raycast;

import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.Collection;

public class AABB {
    public static final Collection<BlockFace> FACES = Arrays.asList(BlockFace.EAST, BlockFace.WEST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.UP, BlockFace.DOWN);

    private final Vector min, max;

    public AABB(Vector min, Vector max) {
        this.min = Vector.getMinimum(min, max);
        this.max = Vector.getMaximum(min, max);
    }

    /**
     * @param i1 Minimum x
     * @param i2 Minimum y
     * @param i3 Minimum z
     * @param a1 Maximum x
     * @param a2 Maximum y
     * @param a3 Maximum z
     */
    public AABB(double i1, double i2, double i3, double a1, double a2, double a3) {
        this(new Vector(i1, i2, i3), new Vector(a1, a2, a3));
    }

    public Vector getMin() {
        return min.clone();
    }

    public Vector getMax() {
        return max.clone();
    }

    public Vector getSize() {
        return getMax().subtract(getMin());
    }

    public Vector getCenter() {
        return getMin().add(getSize().multiply(0.5));
    }

    public BoundedPlane getSide(BlockFace face) {
        // Basis vectors
        Vector n, a, b;

        switch (face) {
            case EAST:
                n = MathUtil.getI();
                a = MathUtil.getJ();
                b = MathUtil.getK();
                break;
            case WEST:
                n = MathUtil.getI().multiply(-1.0);
                a = MathUtil.getJ().multiply(-1.0);
                b = MathUtil.getK();
                break;
            case UP:
                n = MathUtil.getJ();
                a = MathUtil.getK();
                b = MathUtil.getI();
                break;
            case DOWN:
                n = MathUtil.getJ().multiply(-1.0);
                a = MathUtil.getK().multiply(-1.0);
                b = MathUtil.getI();
                break;
            case SOUTH:
                n = MathUtil.getK();
                a = MathUtil.getI();
                b = MathUtil.getJ();
                break;
            case NORTH:
                n = MathUtil.getK().multiply(-1.0);
                a = MathUtil.getI().multiply(-1.0);
                b = MathUtil.getJ();
                break;
            default:
                throw new IllegalArgumentException("Plane face must have a unit vector parallel to a coordinate axis");
        }

        Vector origin = getCenter().add(n.clone().multiply(getSize()).multiply(0.5));

        double uMin, vMin;
        double uMax, vMax;

        if (face.getModX() != 0) {
            uMin = -0.5 * getSize().getY();
            vMin = -0.5 * getSize().getZ();
            uMax = 0.5 * getSize().getY();
            vMax = 0.5 * getSize().getZ();
        } else if (face.getModY() != 0) {
            uMin = -0.5 * getSize().getZ();
            vMin = -0.5 * getSize().getX();
            uMax = 0.5 * getSize().getZ();
            vMax = 0.5 * getSize().getX();
        } else if (face.getModZ() != 0) {
            uMin = -0.5 * getSize().getX();
            vMin = -0.5 * getSize().getY();
            uMax = 0.5 * getSize().getX();
            vMax = 0.5 * getSize().getY();
        } else {
            throw new AssertionError();
        }

        return new BoundedPlane(origin, n, a, b, uMin, vMin, uMax, vMax);
    }
}
