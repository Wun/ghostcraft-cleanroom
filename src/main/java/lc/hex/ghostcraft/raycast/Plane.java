package lc.hex.ghostcraft.raycast;

import org.apache.commons.lang.Validate;
import org.bukkit.util.Vector;

public class Plane {
    private final Vector origin;
    private final Vector normal;
    private final Vector a, b;

    public Plane(Vector origin, Vector normal, Vector a, Vector b) {
        this.origin = origin.clone();
        this.normal = normal.clone().normalize();
        this.a = a.clone().normalize();
        this.b = b.clone().normalize();
        validateVectors();
    }

    public Vector getOrigin() {
        return origin.clone();
    }

    public Vector getNormal() {
        return normal.clone();
    }

    public Vector getA() {
        return a.clone();
    }

    public Vector getB() {
        return b.clone();
    }

    private void validateVectors() {
        Validate.isTrue(MathUtil.areZero(a.dot(b), a.dot(normal), b.dot(normal)), "Basis vectors must be perpendicular");
        Validate.isTrue(normal.equals(a.getCrossProduct(b)), "Basis vectors must be right-handed");
    }
}
