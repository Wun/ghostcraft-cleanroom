package lc.hex.ghostcraft.raycast;

import org.bukkit.util.Vector;

public class BoundedPlane extends Plane {
    private final double uMin, vMin;
    private final double uMax, vMax;

    public BoundedPlane(Vector origin, Vector normal, Vector a, Vector b, double uMin, double vMin, double uMax, double vMax) {
        super(origin, normal, a, b);
        this.uMin = Math.min(uMin, uMax);
        this.uMax = Math.max(uMin, uMax);
        this.vMin = Math.min(vMin, vMax);
        this.vMax = Math.max(vMin, vMax);
    }

    public boolean isInBounds(double u, double v) {
        return u >= uMin && u <= uMax && v >= vMin && v <= vMax;
    }
}
