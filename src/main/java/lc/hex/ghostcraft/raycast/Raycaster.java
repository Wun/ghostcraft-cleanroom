package lc.hex.ghostcraft.raycast;

import org.apache.commons.lang.Validate;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Raycaster<T> {
    public static final double EPSILON = 1e-6;

    private final BoundedLine ray;
    private final Map<T, AABB> boxes = new HashMap<>();
    private final double epsilon;

    public Raycaster(BoundedLine ray, double epsilon) {
        Validate.notNull(ray, "Ray cannot be null");
        this.ray = ray;
        this.epsilon = epsilon;
    }

    public Raycaster(BoundedLine ray) {
        this(ray, EPSILON);
    }

    public void addSubject(T subject, AABB box) {
        Validate.notNull(subject, "Subject cannot be null");
        Validate.notNull(box, "Bounding box cannot be null");
        Vector relPosition = box.getCenter().subtract(ray.getOrigin());
        if (ray.getE().multiply(relPosition.length()).distanceSquared(relPosition) < box.getSize().lengthSquared() * 2) {
            boxes.put(subject, box);
        }
    }

    public RaycastResult<T> raycast() {
        T nearestSubject = null;
        double nearestT = Double.POSITIVE_INFINITY;
        for (Map.Entry<T, AABB> entry : boxes.entrySet()) {
            double t = findNearestIntersection(entry.getValue());
            if (Double.isFinite(t) && t < nearestT) {
                nearestT = t;
                nearestSubject = entry.getKey();
            }
        }
        return new RaycastResult<>(nearestSubject, nearestT);
    }

    private double findNearestIntersection(AABB box) {
        // First check if they are inside the bounding box
        if (ray.getOrigin().isInAABB(box.getMin(), box.getMax())) {
            return ray.getOrigin().distance(box.getCenter());
        }

        // Use standard raycasting procedure
        double nearest = Double.POSITIVE_INFINITY;
        for (BlockFace face : AABB.FACES) {
            BoundedPlane plane = box.getSide(face);
            LinePlaneIntersection intersection = findIntersection(ray, plane);
            if (intersection != null) {
                if (ray.isInBounds(intersection.t) && plane.isInBounds(intersection.u, intersection.v)) {
                    if (intersection.t < nearest) {
                        nearest = intersection.t;
                    }
                }
            }
        }
        return nearest;
    }

    private LinePlaneIntersection findIntersection(Line line, Plane plane) {
        Matrix matrixA = new Matrix().setColumns(line.getE(), plane.getA().multiply(-1.0), plane.getB().multiply(-1.0));
        double detA = matrixA.determinant();

        if (isZero(detA, epsilon)) {
            return null;
        }

        Vector b = plane.getOrigin().subtract(line.getOrigin());

        Matrix tCramer = new Matrix().setColumns(b, plane.getA().multiply(-1.0), plane.getB().multiply(-1.0));
        Matrix uCramer = new Matrix().setColumns(line.getE(), b, plane.getB().multiply(-1.0));
        Matrix vCramer = new Matrix().setColumns(line.getE(), plane.getA().multiply(-1.0), b);
        double t = tCramer.determinant() / detA;
        double u = uCramer.determinant() / detA;
        double v = vCramer.determinant() / detA;

        return new LinePlaneIntersection(t, u, v);
    }

    private static boolean isZero(double d) {
        return isZero(d, EPSILON);
    }

    private static boolean isZero(double d, double epsilon) {
        return d >= -epsilon && d <= epsilon;
    }

    public static class RaycastResult<T> {
        private final Optional<T> target;
        private final double distance;

        public RaycastResult(T target, double distance) {
            this.target = Optional.ofNullable(target);
            this.distance = distance;
        }

        public boolean foundTarget() {
            return target.isPresent();
        }

        public Optional<T> getTarget() {
            return target;
        }

        public double getDistance() {
            return distance;
        }
    }

    private static class LinePlaneIntersection {
        private final double t;
        private final double u, v;

        private LinePlaneIntersection(double t, double u, double v) {
            validateNumbers(t, u, v);
            this.t = t;
            this.u = u;
            this.v = v;
        }
    }

    private static void validateNumbers(double... numbers) {
        for (double d : numbers) {
            Validate.isTrue(!Double.isNaN(d) && Double.isFinite(d));
        }
    }
}
