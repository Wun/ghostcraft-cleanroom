package lc.hex.ghostcraft.raycast;

import org.bukkit.util.Vector;

public class BoundedLine extends Line {
    private final double tMin, tMax;

    public BoundedLine(Vector origin, Vector e, double tMin, double tMax) {
        super(origin, e);
        this.tMin = Math.min(tMin, tMax);
        this.tMax = Math.max(tMin, tMax);
    }

    public boolean isInBounds(double t) {
        return t >= tMin && t <= tMax;
    }
}
