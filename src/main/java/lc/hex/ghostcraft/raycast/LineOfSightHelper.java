package lc.hex.ghostcraft.raycast;

import net.minecraft.server.v1_11_R1.AxisAlignedBB;

import net.minecraft.server.v1_11_R1.IBlockAccess;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.BlockIterator;

public class LineOfSightHelper {
    public static Raycaster.RaycastResult<Entity> getTargetEntity(LivingEntity entity, double range) {
        return getTargetEntity(entity, range, Raycaster.EPSILON);
    }

    public static Raycaster.RaycastResult<Entity> getTargetEntity(LivingEntity entity, double range, double epsilon) {
        BoundedRay ray = new BoundedRay(entity.getEyeLocation().toVector(), entity.getEyeLocation().getDirection(), range);
        Raycaster<Entity> raycaster = new Raycaster<>(ray);
        entity.getWorld().getLivingEntities().stream().filter(subject -> subject != entity).forEach(subject -> {
            AxisAlignedBB nmsBox = ((CraftEntity) subject).getHandle().getBoundingBox();
            AABB box = new AABB(nmsBox.a, nmsBox.b, nmsBox.c, nmsBox.d, nmsBox.e, nmsBox.f);
            raycaster.addSubject(subject, box);
        });
        return raycaster.raycast();
    }

    public static Raycaster.RaycastResult<Block> getTargetBlock(LivingEntity entity, int maxDistance) {
        return getTargetBlock(entity, maxDistance, Raycaster.EPSILON);
    }

    public static Raycaster.RaycastResult<Block> getTargetBlock(LivingEntity entity, int maxDistance, double epsilon) {
        BlockIterator it = new BlockIterator(entity, maxDistance);
        BoundedRay ray = new BoundedRay(entity.getEyeLocation().toVector(), entity.getEyeLocation().getDirection(), maxDistance);

        while (it.hasNext()) {
            Block b = it.next();
            if (!b.getType().isSolid()) {
                continue;
            }

            net.minecraft.server.v1_11_R1.World nmsWorld = ((CraftWorld) entity.getWorld()).getHandle();
            net.minecraft.server.v1_11_R1.BlockPosition nmsPos = new net.minecraft.server.v1_11_R1.BlockPosition(b.getX(), b.getY(), b.getZ());
            net.minecraft.server.v1_11_R1.IBlockData nmsBlockData = nmsWorld.getType(nmsPos);
            net.minecraft.server.v1_11_R1.AxisAlignedBB nmsAABB = nmsBlockData.getBlock().a(nmsBlockData, (IBlockAccess) nmsWorld, nmsPos);

            Raycaster<Block> raycaster = new Raycaster<>(ray, epsilon);
            raycaster.addSubject(b, new AABB(nmsAABB.a, nmsAABB.b, nmsAABB.c, nmsAABB.d, nmsAABB.e, nmsAABB.f));
            Raycaster.RaycastResult<Block> result = raycaster.raycast();
            if (result.foundTarget()) {
                return result;
            }
        }

        return new Raycaster.RaycastResult<>(null, 0);
    }
}
