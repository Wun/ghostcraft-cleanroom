package lc.hex.ghostcraft.raycast;

import org.bukkit.util.Vector;

public class Matrix {
    private double[][] value = new double[3][3];

    public Matrix setColumns(Vector c1, Vector c2, Vector c3) {
        value[0][0] = c1.getX();
        value[1][0] = c1.getY();
        value[2][0] = c1.getZ();

        value[0][1] = c2.getX();
        value[1][1] = c2.getY();
        value[2][1] = c2.getZ();

        value[0][2] = c3.getX();
        value[1][2] = c3.getY();
        value[2][2] = c3.getZ();

        return this;
    }

    public Matrix setRows(Vector r1, Vector r2, Vector r3) {
        value[0][0] = r1.getX();
        value[0][1] = r1.getY();
        value[0][2] = r1.getZ();

        value[1][0] = r2.getX();
        value[1][1] = r2.getY();
        value[1][2] = r2.getZ();

        value[2][0] = r3.getX();
        value[2][1] = r3.getY();
        value[2][2] = r3.getZ();

        return this;
    }

    public double determinant() {
        double a, b, c; // Determinants of minors of first row

        a = value[1][1] * value[2][2] - value[2][1] * value[1][2];
        b = value[1][0] * value[2][2] - value[2][0] * value[1][2];
        c = value[1][0] * value[2][1] - value[2][0] * value[1][1];

        return value[0][0] * a - value[0][1] * b + value[0][2] * c;
    }
}
