package lc.hex.ghostcraft.raycast;

import org.bukkit.util.Vector;

public class Line {
    private final Vector origin;
    private final Vector e;

    public Line(Vector origin, Vector e) {
        this.origin = origin.clone();
        this.e = e.clone();
    }

    public Vector getOrigin() {
        return origin.clone();
    }

    public Vector getE() {
        return e.clone();
    }

    public Vector getPointAt(double t) {
        return getOrigin().add(getE().multiply(t));
    }
}
