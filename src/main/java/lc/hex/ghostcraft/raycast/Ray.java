package lc.hex.ghostcraft.raycast;

import org.bukkit.util.Vector;

public class Ray extends BoundedLine {
    public Ray(Vector origin, Vector e) {
        super(origin, e, 0.0, Double.POSITIVE_INFINITY);
    }
}
