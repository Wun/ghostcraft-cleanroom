package lc.hex.ghostcraft.raycast;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import java.util.Set;
import java.util.stream.Collectors;

public final class MathUtil {
    public static final double EPSILON = 1.0e-6;

    public static Vector getI() {
        return new Vector(1.0, 0.0, 0.0);
    }

    public static Vector getJ() {
        return new Vector(0.0, 1.0, 0.0);
    }

    public static Vector getK() {
        return new Vector(0.0, 0.0, 1.0);
    }

    private MathUtil() {
    }

    public static boolean isZero(double d /* boobs */) {
        return d >= -EPSILON && d <= EPSILON;
    }

    public static boolean areZero(double... ds) {
        for (double d : ds) {
            if (!isZero(d)) {
                return false;
            }
        }
        return true;
    }

    public static Vector crossProduct(Vector a, Vector b) {
        Vector c = a.clone();
        c.crossProduct(b);
        return c;
    }

    public static long gcd(long a, long b)
    {
        while (b > 0)
        {
            long temp = b;
            b = a % b; // % is remainder
            a = temp;
        }
        return a;
    }

    public static long gcd(long... input)
    {
        long result = input[0];
        for(int i = 1; i < input.length; i++) result = gcd(result, input[i]);
        return result;
    }

    public static long lcm(long a, long b)
    {
        return a * (b / gcd(a, b));
    }

    public static long lcm(long... input)
    {
        long result = input[0];
        for(int i = 1; i < input.length; i++) result = lcm(result, input[i]);
        return result;
    }

    public static long lcm(Long a, Long b) {
        return lcm(a, b);
    }

    public static long lcm(Long... input) {
        return lcm(input);
    }

    public static Set<Entity> getNearbyEntitiesInSphere(Class<? extends Entity> filter, Location origin, double radius) {
        return origin.getWorld().getEntities().stream().filter(entity -> filter.isAssignableFrom(entity.getClass()))
                .filter(entity -> origin.distanceSquared(entity.getLocation()) < radius * radius).collect(Collectors.toSet());
    }

}
