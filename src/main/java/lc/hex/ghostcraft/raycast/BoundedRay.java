package lc.hex.ghostcraft.raycast;

import org.bukkit.util.Vector;

public class BoundedRay extends BoundedLine {
    public BoundedRay(Vector origin, Vector e, double tMax) {
        super(origin, e, 0.0, tMax);
    }
}
