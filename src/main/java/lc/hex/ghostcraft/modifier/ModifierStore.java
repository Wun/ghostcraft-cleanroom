package lc.hex.ghostcraft.modifier;

import lc.hex.ghostcraft.GhostcraftPlugin;
import lc.hex.ghostcraft.roles.GCPlayerRole;
import lc.hex.ghostcraft.roles.impl.GCPlayer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public class ModifierStore {
    @Getter
    private final File file;
    private YamlConfiguration configFile;

    Map<String, Double> modifiers = new HashMap<>();

    public void load() {
        try {
            if (!file.exists()) {
                Files.createDirectories(file.getParentFile().toPath());
                file.createNewFile();
                IOUtils.copy(GhostcraftPlugin.class.getResourceAsStream("/modifiers.yml"), new FileOutputStream(file));
            }
        configFile = new YamlConfiguration();
        configFile.load(file);
        } catch (InvalidConfigurationException | IOException e) {
            e.printStackTrace();
        }
        configFile.getConfigurationSection("modifiers").getValues(false).forEach((mod, val) -> {
            modifiers.put(mod, (Double) val);
            GhostcraftPlugin.instance().getLogger().info("Registered modifier " + mod + " = " + val);
        });
    }

    public void save() {
        modifiers.forEach((mod, val) -> configFile.set("modifiers." + mod, val));
        try {
            configFile.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double getModifier(String key) {
        return modifiers.get(key);
    }

    public void setModifier(String key, double value) {
        modifiers.put(key, value);
    }

    public boolean modifierExists(String key) {
        return modifiers.containsKey(key);
    }

    public void sync() {
        GCPlayer.players.values().stream().map(gcplayer -> gcplayer.getRole().getClass()).forEach(this::sync);
    }

    public void sync(Class<? extends GCPlayerRole> clazz) {
        for (Field f : clazz.getDeclaredFields()) {
            if (modifiers.keySet().contains(f.getName())) {
                boolean flag = f.isAccessible();
                f.setAccessible(true);
                try {
                    f.set(null, getModifier(f.getName()));
                    Bukkit.broadcastMessage(ChatColor.GOLD + " -- " + ChatColor.LIGHT_PURPLE + "synchronize modifier " + f.getName());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                f.setAccessible(flag);
            }
        }
    }
}
