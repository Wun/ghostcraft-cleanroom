package lc.hex.ghostcraft.modifier;

import com.torchmind.minecraft.annotation.permission.Permission;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

@com.torchmind.minecraft.annotation.command.Command(name = "modifier", aliases = "mod", permission = "ghostcraft.modifiers")
@Permission(name = "ghostcraft.modifiers")
@RequiredArgsConstructor
public class ModifierCommand implements CommandExecutor {
    private final ModifierStore store;

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (args.length == 0) {
            return false;
        } else if (args.length == 1) {
            if (args[0].equals("sync")) {
                store.sync();
            } else {
                if (!store.modifierExists(args[0])) {
                    commandSender.sendMessage(ChatColor.RED + "No such modifier " + ChatColor.GOLD + args[0]);
                }
                commandSender.sendMessage(ChatColor.GOLD + args[0] + ": " + ChatColor.GREEN + store.getModifier(args[0]));
            }
        } else if (args.length == 2) {
            if (!store.modifierExists(args[0])) {
                commandSender.sendMessage(ChatColor.RED + "No such modifier " + ChatColor.GOLD + args[0]);
            }
            store.setModifier(args[0], Double.parseDouble(args[1]));
            Bukkit.broadcastMessage(ChatColor.GOLD + "-" + commandSender.getName() + "- " + ChatColor.LIGHT_PURPLE + "set modifier " + args[0] + " " + args[1]);
        }
        return true;
    }
}
