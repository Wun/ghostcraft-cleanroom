package lc.hex.ghostcraft.roles;

import org.bukkit.event.Event;

/**
 * Handles the actual invocation of an ability.
 */
@FunctionalInterface
public interface AbilityUse {
    /**
     * See {@link GCPlayerRole#useAbility(Ability, Ability.Trigger, Event)}
     */
    void use(AbilityUser user, Ability.Trigger trigger, Event triggerEvent);
}
