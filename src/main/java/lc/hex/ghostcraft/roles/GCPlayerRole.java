package lc.hex.ghostcraft.roles;

import lc.hex.ghostcraft.GhostcraftPlugin;
import lc.hex.ghostcraft.roles.impl.GCPlayer;
import org.bukkit.event.Event;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import lombok.Getter;

/**
 * Superclass for any player wrapper that should be using abilities.
 * @param <T> Should be the subclass of this that represents the role.
 */
public abstract class GCPlayerRole<T extends AbilityUser> implements AbilityUser<T> {
    static HashMap<Ability.Trigger, Collection<Ability>> registry = new HashMap<>();
    private static Set<Ability> registeredAbilities = new HashSet<>();

    static {
        for (Ability.Trigger trigger : Ability.Trigger.values()) {
            registry.put(trigger, new HashSet<>());
        }
    }

    @Getter
    private final GCPlayer player;
    Map<Ability, Long> abilities;

    {
        this.abilities = new ConcurrentHashMap<>();
        GhostcraftPlugin.instance().getModifierStore().sync(this.getClass());
    }

    protected GCPlayerRole(GCPlayer player) {
        this.player = player;
        this.player.respawn();
    }

    /**
     * Get abilities that can be used by this role
     * @return A collection of abilities that <i>is</i> backed by the cooldown map.
     */
    public Collection<Ability> getActiveAbilities() {
        return abilities.keySet();
    }

    /**
     * Get abilities that are available with regards to their cooldown.
     * @return A collection of abilities that isn't backed by the cooldown map.
     */
    public Collection<Ability> getAvailableAbilities() {
        return abilities.keySet().stream()
                .filter(ability -> abilities.get(ability) == 0)
                .collect(Collectors.toSet());
    }

    @Override
    public long getCooldown(Ability ability) {
        return abilities.get(ability);
    }

    @Override
    public void setCooldown(Ability ability, long cooldown) {
        abilities.put(ability, cooldown);
        if (ability.getTriggers().contains(Ability.Trigger.AVAILABLE) && cooldown <= 0) {
            useAbility(ability, Ability.Trigger.AVAILABLE, null);
        }
    }

    /**
     * Delegates ability invocations from {@link AbilityInvoker} to the ability
     * instance. Will return an {@link lc.hex.ghostcraft.roles.AbilityUser.UseResult}
     * determining what happened during the invocation.
     * @param ability The ability to invoke.
     * @param trigger What triggered the ability. This is determined inside the {@link AbilityInvoker}
     * @param event The event that triggered this ability. The type of this should be determined by the trigger.
     * @return Details about the ability usage.
     */
    @Override
    public UseResult useAbility(Ability ability, Ability.Trigger trigger, Event event) {
        if (registeredAbilities.contains(ability) && abilities.containsKey(ability)) {
            if (!ability.filter().test(this)) {
                return UseResult.FILTERED;
            }
            long currentCooldown = abilities.get(ability);
            if (currentCooldown > 0) {
                return UseResult.ERR_COOLDOWN;
            }

            if (!AbilityUseEvent.call(this, ability, trigger, event).isCancelled()) {
                ability.use(this, trigger, event);
                abilities.put(ability, ability.getFullCooldown());
                return UseResult.SUCCESS;
            }
        }
        return UseResult.ERR_UNREGISTERED;
    }

    /**
     * Registers an ability for use by this specific user
     * @param ability The ability to register
     * @param triggers The triggers for which to trigger this ability.
     */
    @Override
    public void activateAbility(Ability ability, Ability.Trigger... triggers) {
        if (!registeredAbilities.contains(ability)) {
            for (Ability.Trigger trigger : triggers) {
                Collection<Ability> list;
                if (!registry.containsKey(trigger)) {
                    list = new HashSet<>();
                    list.add(ability);
                } else {
                    list = registry.get(trigger);
                }
                list.add(ability);
                registry.put(trigger, list);
            }
            registeredAbilities.add(ability);
        }
        abilities.put(ability, 0L);
    }

    @Override
    public void deactivateAbility(Ability ability) {
        abilities.remove(ability);
    }
}
