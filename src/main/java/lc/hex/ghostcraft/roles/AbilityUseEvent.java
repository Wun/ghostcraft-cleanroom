package lc.hex.ghostcraft.roles;

import org.bukkit.Bukkit;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.Optional;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Called when an ability is used.
 */
@RequiredArgsConstructor
public class AbilityUseEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    @Setter
    private boolean cancelled = false;
    @Getter
    private final AbilityUser user;
    @Getter
    private final Ability ability;
    @Getter
    private final Ability.Trigger trigger;
    @Getter
    private final Optional<Event> event;

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public static AbilityUseEvent call(AbilityUser user, Ability ability, Ability.Trigger trigger, Event event1) {
        AbilityUseEvent event = new AbilityUseEvent(user, ability, trigger, Optional.ofNullable(event1));
        Bukkit.getPluginManager().callEvent(event);
        return event;
    }
}
