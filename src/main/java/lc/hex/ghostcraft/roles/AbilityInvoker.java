package lc.hex.ghostcraft.roles;

import lc.hex.ghostcraft.roles.impl.GCPlayer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import static lc.hex.ghostcraft.roles.Ability.Trigger.ATTACK;
import static lc.hex.ghostcraft.roles.Ability.Trigger.CUSTOM;
import static lc.hex.ghostcraft.roles.Ability.Trigger.DAMAGE;
import static lc.hex.ghostcraft.roles.Ability.Trigger.FLY;
import static lc.hex.ghostcraft.roles.Ability.Trigger.HIT;
import static lc.hex.ghostcraft.roles.Ability.Trigger.ITEM_L;
import static lc.hex.ghostcraft.roles.Ability.Trigger.ITEM_R;
import static lc.hex.ghostcraft.roles.Ability.Trigger.PRESSURE;
import static lc.hex.ghostcraft.roles.Ability.Trigger.SNEAK;
import static lc.hex.ghostcraft.roles.Ability.Trigger.SPAWN;
import static lc.hex.ghostcraft.roles.GCPlayerRole.registry;

/**
 * Handles transcription of events into ability invocations.
 * See {@link lc.hex.ghostcraft.roles.Ability.Trigger} for a list
 * of supported triggers.
 */
public class AbilityInvoker implements Listener {

    public void invoke(Player player, Ability.Trigger trigger, Event event) {
        if (GCPlayer.players.containsKey(player)) {
            registry.get(trigger).forEach(ability ->
                    GCPlayer.players.get(player).getRole().useAbility(ability, trigger, event).name());
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Ability.Trigger trigger = CUSTOM;
        Player player = event.getPlayer();
        switch (event.getAction()) {
            case LEFT_CLICK_AIR:
            case LEFT_CLICK_BLOCK:
                if (player.getInventory().getItemInMainHand() != null && player.getInventory().getItemInMainHand().getType() != Material.AIR) {
                    trigger = ITEM_L;
                } else {
                    trigger = HIT;
                }
                break;
            case RIGHT_CLICK_AIR:
            case RIGHT_CLICK_BLOCK:
                trigger = ITEM_R; // We don't get this unless there's an item in hand.
                break;
            case PHYSICAL:
                trigger = PRESSURE;
                break;
        }
        invoke(player, trigger, event);
    }

    @EventHandler(ignoreCancelled = true)
    public void onToggleSneak(PlayerToggleSneakEvent event) {
        invoke(event.getPlayer(), SNEAK, event);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        invoke(event.getPlayer(), SPAWN, event);
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            invoke((Player) event.getEntity(), DAMAGE, event);
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            invoke((Player) event.getDamager(), ATTACK, event);
        }
    }

    @EventHandler
    public void onDoubleJump(PlayerToggleFlightEvent event) {
        invoke(event.getPlayer(), FLY, event);
    }

    /**
     * Used to decrement cooldowns. Since it's only one thread that runs every
     * second, this'll cause cooldowns to be +/- 1 second off, most of the time.
     * I can make a better one later.
     */
    public static class CooldownHandler implements Runnable {
        @Override
        @SuppressWarnings("unchecked")
        public void run() {
            GCPlayer.players.values().stream().map(GCPlayer::getRole)
                    .forEach(role -> role.abilities
                            .forEach((ability, cooldown) -> role.setCooldown((Ability) ability, ((long) cooldown) - 1)));
        }
    }
}
