package lc.hex.ghostcraft.roles;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.function.Consumer;

import javax.annotation.Nullable;

import lc.hex.ghostcraft.GhostcraftPlugin;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Stores a collection of useful ability presets.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AbilityPresets {

    /**
     * An ability in which an item is thrown (and its stack is decremented) with the given force
     * and y-offset, then performs an action. The grenade is triggered by a right click.
     * @param item The item type for this ability to be used.
     * @param delay The delay (in ticks) before this grenade triggers
     * @param force The force at which the grenade is propelled forward from the player
     * @param pitch The y-offset of the grenade
     * @param task The task to be run when the grenade detonates after the delay
     * @param postThrown The task to be run once the item is thrown. Can be null.
     * @return An ability builder (to perform filtering, etc.)
     */
    public static Ability.Builder grenade(Material item, long delay, double force, double pitch, Consumer<Item> task, @Nullable Consumer<Item> postThrown) {
        return Ability.builder()
                .item(item)
                .trigger(Ability.Trigger.ITEM_R)
                .action((user, trigger, triggerEvent) -> {
                    final Player p = user.getPlayer().getHandle();
                    final ItemStack grenadeStack = p.getInventory().getItemInMainHand();
                    final ItemStack singleGrenade = grenadeStack.clone();
                    singleGrenade.setAmount(1);
                    grenadeStack.setAmount(grenadeStack.getAmount() - 1);
                    Item thrown = p.getWorld().dropItem(p.getEyeLocation().subtract(0, 0.88d, 0), singleGrenade);
                    thrown.setPickupDelay(Integer.MAX_VALUE);
                    thrown.getVelocity().add(p.getEyeLocation().getDirection()
                            .clone().add(new Vector(0, pitch, 0)).multiply(force));
                    if (postThrown != null) {
                        postThrown.accept(thrown);
                    }
                    Bukkit.getScheduler().scheduleSyncDelayedTask(GhostcraftPlugin.instance(),
                            () -> task.accept(thrown), delay);
                });
    }
}
