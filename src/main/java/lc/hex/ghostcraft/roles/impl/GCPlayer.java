package lc.hex.ghostcraft.roles.impl;

import lc.hex.ghostcraft.roles.GCPlayerRole;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

import lc.hex.ghostcraft.game.GCGame;
import lc.hex.ghostcraft.game.Team;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Constructed on connect, modified when game is joined. Is currently
 * just a simple player wrapper.
 */
@Getter
public class GCPlayer {
    public static Map<Player, GCPlayer> players = new HashMap<>();

    private final Player handle;
    @Setter(AccessLevel.PACKAGE)
    private GCGame game;
    @Setter(AccessLevel.PACKAGE)
    private Team team;
    @Setter
    private GCPlayerRole role;

    public GCPlayer(Player handle) {
        this.handle = handle;
        players.put(handle, this);
    }

    /**
     * Cleans up player's status. Doesn't handle teleportation to
     * spawnpoints.
     */
    public void respawn() {
        handle.spigot().respawn();
        handle.setWalkSpeed(0.2f);
        handle.setFlySpeed(0.2f);
        handle.getActivePotionEffects().removeIf(effect -> true);
    }
}
