package lc.hex.ghostcraft.roles.impl;

import lc.hex.ghostcraft.roles.Ability;
import lc.hex.ghostcraft.roles.AbilityPresets;
import lc.hex.ghostcraft.roles.GCPlayerRole;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

import lc.hex.ghostcraft.GhostcraftPlugin;
import lc.hex.ghostcraft.command.GCCommand;
import lc.hex.ghostcraft.game.Team;
import lc.hex.ghostcraft.raycast.LineOfSightHelper;
import lc.hex.ghostcraft.raycast.Raycaster;

import java.util.Optional;

/**
 * Role handler for a ghost. Stores all abilities pertaining to it.
 */
public class Ghost extends GCPlayerRole<Ghost> {
    public static double GHOST_LEAP_HSCAL,
            GHOST_LEAP_YADD,
            GHOST_LEAP_ASCAL,
            GHOST_STAB_RANGE,
            GHOST_STAB_EPSILON,
            GHOST_SPEED_BOOST,
            GHOST_THROW_HSCAL,
            GHOST_THROW_YADD,
            GHOST_THROW_ASCAL;
    private double smokeDensity = 2;
    private long smokePeriod = 25L;

    /**
     * The ghost's leap ability, triggered by right-clicking a wood sword or shears.
     * Constants for movement were defined in {@link GCCommand} for fine-tuning purposes.
     */
    public static Ability LEAP = Ability.builder()
            .cooldown(35L)
            .item(Material.WOOD_SWORD, Material.SHEARS)
            .filter(user -> user.getPlayer().getTeam() == Team.GHOST)
            .trigger(Ability.Trigger.ITEM_R)
            .action((user, trigger, triggerEvent) -> {
                Bukkit.broadcastMessage("Leap!");
                Player p = user.getPlayer().getHandle();
                p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ENDERDRAGON_FLAP, 1, 1);
                Vector orig = p.getLocation().getDirection();
                orig.multiply(GHOST_LEAP_HSCAL);
                Vector up = new Vector(0, GHOST_LEAP_YADD, 0);
                orig.add(up);
                orig.multiply(GHOST_LEAP_ASCAL);
                p.setVelocity(orig);
            }).build();

    /**
     * The ghost's stab ability, triggered by left-clicking with shears.
     * Effects are imprecise, and raycasting is too precise.
     */
    public static Ability STAB = Ability.builder()
            .cooldown(40L)
            .item(Material.SHEARS)
            .filter(user -> user.getPlayer().getTeam() == Team.GHOST)
            .trigger(Ability.Trigger.ITEM_L)
            .action((user, trigger, triggerEvent) -> {
                Player p = user.getPlayer().getHandle();
                p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_PIG_ANGRY, 1F, 1F);
                p.getWorld().spigot().playEffect(p.getEyeLocation(), Effect.TILE_DUST, Material.OBSIDIAN.getId(), 0, 0f, -0.45f, 0f, 20, 15, 1);
                Bukkit.getScheduler().scheduleSyncDelayedTask(GhostcraftPlugin.instance(), () -> {
                    p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_PIG_HURT, 1F, 1F);
                    p.getWorld().spigot().playEffect(p.getEyeLocation(), Effect.TILE_DUST, Material.PUMPKIN.getId(), 0, 0f, -0.45f, 0f, 20, 15, 1);
                    Raycaster.RaycastResult<Entity> result = LineOfSightHelper.getTargetEntity(p, GHOST_STAB_RANGE, GHOST_STAB_EPSILON);
                    if (result.foundTarget() && result.getTarget().get() instanceof LivingEntity) {
                        ((LivingEntity)result.getTarget().get()).setHealth(0);
                        Bukkit.broadcastMessage("Stab!");
                        p.getWorld().playSound(result.getTarget().get().getLocation(), Sound.ENTITY_PLAYER_ATTACK_STRONG, 1, 1);
                    }
                }, 22L);
            }).build();

    /**
     * A passive ability that prevents the ghost from taking fall damage.
     */
    public static Ability NOFALL = Ability.builder()
            .cooldown(0L) // not needed
            .filter(user -> user.getPlayer().getTeam() == Team.GHOST)
            .trigger(Ability.Trigger.DAMAGE)
            .action((user, trigger, triggerEvent) -> {
                EntityDamageEvent event = (EntityDamageEvent) triggerEvent;
                if (event.getCause() == EntityDamageEvent.DamageCause.FALL) {
                    event.setCancelled(true);
                }
            }).build();

    /**
     * A passive ability that makes the Ghost regain 1.0 health whenever damage is dealt
     * to an enemy.
     */
    public static Ability LIFEDRAIN = Ability.builder()
            .filter(user -> user.getPlayer().getTeam() == Team.GHOST)
            .trigger(Ability.Trigger.ATTACK)
            .action((user, trigger, triggerEvent) -> {
                Player p = user.getPlayer().getHandle();
                p.setHealth(Math.min(20.0d, p.getHealth() + 1.0d));
            }).build();

    /**
     * A passive ability that makes the Ghost faster than normal
     * players.
     */
    public static Ability SPEED_PASSIVE = Ability.builder()
            .filter(user -> user.getPlayer().getTeam() == Team.GHOST)
            .trigger(Ability.Trigger.SPAWN)
            .action((user, trigger, triggerEvent) -> {
                user.getPlayer().getHandle().setWalkSpeed((float) GHOST_SPEED_BOOST);
                Bukkit.broadcastMessage("Fast!");
            })
            .build();

    public static Ability FRAG_GRENADE = AbilityPresets.grenade(Material.MAGMA_CREAM, 600L, 5.0D, 0.1D, item -> {
        Bukkit.broadcastMessage("Explode!");
        World world = item.getWorld();
        Location l = item.getLocation();
        world.createExplosion(l.getX(), l.getY(), l.getZ(), 6.3f, false, false);
    }, item -> {
        Bukkit.broadcastMessage("Thrown!");
    }).build();

    public static Ability THROW_BLOCK = Ability.builder()
            .cooldown(25L)
            .team(Team.GHOST)
            .trigger(Ability.Trigger.HIT)
            .trigger(Ability.Trigger.ITEM_L)
            .action((user, trigger, triggerEvent) -> {
                Player p = user.getPlayer().getHandle();
                Optional<Block> targetBlock = LineOfSightHelper.getTargetBlock(p, 4).getTarget();
                targetBlock.ifPresent(block -> {
                    Bukkit.broadcastMessage(block.getType().name());
                    block.setType(Material.AIR);
                    block.getWorld().playSound(block.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_DOOR_WOOD, 1F, 1F);
                    FallingBlock fallingBlock = (FallingBlock) block.getWorld().spawnEntity(block.getLocation(), EntityType.FALLING_BLOCK);
                    fallingBlock.setHurtEntities(true);
                    Vector horiz = p.getLocation().getDirection().normalize().multiply(GHOST_THROW_HSCAL);
                    Vector up = new Vector(0, GHOST_THROW_YADD, 0);
                    horiz.add(up);
                    horiz.multiply(GHOST_THROW_ASCAL);
                    fallingBlock.setVelocity(horiz);
                });
            }).build();

    public Ability SMOKE = Ability.builder()
            .cooldown(smokePeriod)
            .team(Team.GHOST)
            .trigger(Ability.Trigger.AVAILABLE)
            .action((user, trigger, triggerEvent) -> {
                Player p = user.getPlayer().getHandle();
                Location loc = p.getEyeLocation();
                while (loc.getY() > p.getLocation().getY()) {
                    p.getWorld().playEffect(loc, Effect.SMOKE, 1);
                    loc.subtract(0, (p.getEyeLocation().getY() - p.getLocation().getY()) / ((Ghost) user).smokeDensity, 0);
                }
            }).build();

    /**
     * Construct a new instance of this role associated with
     * the given wrapped player.
     * @param handle The wrapped player.
     */
    public Ghost(GCPlayer handle) {
        super(handle);
        handle.setTeam(Team.GHOST);
        this.activateAbility(LEAP, Ability.Trigger.ITEM_R);
        this.activateAbility(STAB, Ability.Trigger.ITEM_L);
        this.activateAbility(NOFALL, Ability.Trigger.DAMAGE);
        this.activateAbility(LIFEDRAIN, Ability.Trigger.ATTACK);
        this.activateAbility(FRAG_GRENADE, Ability.Trigger.ITEM_R);
        this.activateAbility(SPEED_PASSIVE, Ability.Trigger.SPAWN);
        this.activateAbility(THROW_BLOCK, Ability.Trigger.HIT, Ability.Trigger.ITEM_L);
        this.activateAbility(SMOKE, Ability.Trigger.AVAILABLE);
    }
}
