package lc.hex.ghostcraft.roles;

import lc.hex.ghostcraft.game.Team;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Represents an ability. As far as design philosophy goes, Ability is
 * designed for functional use. There should be one and only one instance
 * of each ability that has the same characteristics, and that instance
 * should be able to handle usages from any given user or trigger.
 */
public interface Ability extends AbilityUse {
    Collection<Trigger> getTriggers();

    /**
     * Get how long one must wait between usages.
     * @return The full cooldown in seconds
     */
    long getFullCooldown();

    /**
     * Filters the ability invocation to filter junk invocations.
     * @return A predicate that returns false if the ability should not be invoked.
     */
    Predicate<AbilityUser> filter();

    enum Trigger {
        ITEM_L, ITEM_R, SNEAK, SPAWN, HIT, DAMAGE, ATTACK, PRESSURE, FLY, CUSTOM, AVAILABLE
    }

    /**
     * Creates a builder for a new ability. This is the preferred way to
     * construct new abilities.
     * @return An ability builder
     */
    static Builder builder() {
        return new Builder();
    }

    /**
     * An ability builder.
     * TODO: Implement filters for cancellable events.
     */
    public class Builder {
        private long cooldown = 0L;
        private Set<Trigger> triggers = new HashSet<>();
        private Set<Predicate<AbilityUser>> filters = new HashSet<>();
        private Set<AbilityUse> secondaryTask = new HashSet<>(); // These run first.
        private AbilityUse function;

        /**
         * Sets the full cooldown of the ability (defaults to 0)
         * @param cooldown The cooldown in seconds
         * @return The same builder
         */
        public Builder cooldown(long cooldown) {
            this.cooldown = cooldown;
            return this;
        }

        public Builder team(Team team) {
            return this.filter(user -> user.getPlayer().getTeam() == team);
        }

        /**
         * Adds a trigger to this ability. Not fully implemented.
         * @param trigger The trigger to add
         * @return The same builder
         */
        public Builder trigger(Trigger trigger) {
            triggers.add(trigger);
            return this;
        }

        /**
         * Adds an item filter to the ability. If the using player
         * doesn't have one of the given item types in his/her hand,
         * the invocation is filtered.
         * @param materials An array of legal materials
         * @return The same builder
         */
        public Builder item(Material... materials) {
            return this.filterPlayer(player -> {
                Material inHand = player.getInventory().getItemInMainHand().getType();
                for (Material material : materials) {
                    if (inHand == material) {
                        return true;
                    }
                }
                return false;
            });
        }

        /**
         * Adds a secondary task to be run <i>before</i> the primary task.
         * @param use The task
         * @return The same builder
         */
        public Builder secondaryTask(AbilityUse use) {
            secondaryTask.add(use);
            return this;
        }

        /**
         * Filters based on the player's role.
         * @param filter A predicate that returns false if invocation is to cease.
         * @return The same builder
         */
        public Builder filter(Predicate<AbilityUser> filter) {
            filters.add(filter);
            return this;
        }

        /**
         * A convenience method to filter based on the bukkit Player
         * @param filter A predicate that returns false if the invocation is to cease.
         * @return The same builder
         */
        public Builder filterPlayer(Predicate<Player> filter) {
            filters.add(user -> filter.test(user.getPlayer().getHandle()));
            return this;
        }

        /**
         * This is where the actual invocation of the ability is defined.
         * @param function The function to execute. Can be a lambda expression.
         * @return The same builder.
         */
        public Builder action(AbilityUse function) {
            this.function = function;
            return this;
        }

        /**
         * Build the ability
         * @return An instance of {@link Ability} to be used throughout the program.
         */
        public Ability build() {
            return new Ability() {
                @Override
                public Collection<Trigger> getTriggers() {
                    return triggers;
                }

                @Override
                public long getFullCooldown() {
                    return cooldown;
                }

                @Override
                public Predicate<AbilityUser> filter() {
                    return (user -> {
                        for (Predicate<AbilityUser> filter : filters) {
                            if (!filter.test(user)) {
                                return false;
                            }
                        }
                        return true;
                    });
                }

                @Override
                public void use(AbilityUser user, Trigger trigger, Event triggerEvent) {
                    secondaryTask.forEach(action -> action.use(user, trigger, triggerEvent));
                    function.use(user, trigger, triggerEvent);
                }
            };
        }
    }
}
