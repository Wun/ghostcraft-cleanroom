package lc.hex.ghostcraft.roles;

import lc.hex.ghostcraft.roles.impl.GCPlayer;
import org.bukkit.event.Event;

import java.util.Collection;

/**
 * Base interface for something that can use an ability.
 * Abstracted out for future-proofing.
 * @param <T> The type of ability user subclass.
 */
public interface AbilityUser<T> {
    Collection<Ability> getActiveAbilities();
    long getCooldown(Ability ability);
    void setCooldown(Ability ability, long cooldown);

    void activateAbility(Ability ability, Ability.Trigger... triggers);
    void deactivateAbility(Ability ability);
    UseResult useAbility(Ability ability, Ability.Trigger trigger, Event event);
    GCPlayer getPlayer(); // This really breaks abstraction, but I'm tired.

    /**
     * The result of an ability invocation.
     */
    enum UseResult {
        /**
         * Ability cast succeeded fully.
         */
        SUCCESS,
        /**
         * Ability would have been used used if not for one of its filters.
         */
        FILTERED,
        /**
         * Ability was on cooldown.
         */
        ERR_COOLDOWN,
        /**
         * Ability was unregistered for the user's role.
         */
        ERR_UNREGISTERED,
        /**
         * Miscellaneous bad things.
         */
        ERR_INVALID
    }
}
