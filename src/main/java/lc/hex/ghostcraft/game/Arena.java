package lc.hex.ghostcraft.game;

import org.bukkit.Location;
import org.bukkit.World;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(of = "name")
public class Arena {
    private String name;
    private World world;
    private Location minX, minZ, maxX, maxZ;
}
