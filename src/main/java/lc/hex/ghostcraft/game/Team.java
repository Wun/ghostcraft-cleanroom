package lc.hex.ghostcraft.game;

/**
 * What team a player is on.
 */
public enum Team {
    /**
     * Player is a ghost.
     */
    GHOST,
    /**
     * Player is a survivor. All survivors will have access to
     * the same abilities, but kit loadouts will be determined
     * somewhere else.
     */
    SURVIVOR,
    /**
     * Player is in the lobby. This is to prevent some of the bugs
     * I noticed while playing the original Ghostcraft.
     */
    LOBBY
}
