package lc.hex.ghostcraft.game;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import lc.hex.ghostcraft.roles.impl.GCPlayer;
import lombok.Getter;

/**
 * TODO this is currently simulated inside {@link lc.hex.ghostcraft.command.GCCommand}
 */
public class GCGame {
    private Map<GCPlayer, Team> players;
    @Getter
    private Arena arena;

    public GCGame(Arena arena) {
        this.arena = arena;
        this.players = new HashMap<>();
    }

    public Collection<GCPlayer> getPlayers() {
        return players.keySet();
    }
}
