package lc.hex.ghostcraft;

import com.torchmind.minecraft.annotation.Plugin;
import com.torchmind.minecraft.annotation.command.Command;
import com.torchmind.minecraft.annotation.command.Commands;
import com.torchmind.minecraft.annotation.permission.Permission;
import com.torchmind.minecraft.annotation.permission.Permissions;

import lc.hex.ghostcraft.modifier.ModifierCommand;
import lc.hex.ghostcraft.modifier.ModifierStore;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import lc.hex.ghostcraft.command.GCCommand;
import lc.hex.ghostcraft.effect.ParticleAgent;
import lombok.Getter;

import java.io.File;

@Plugin(name = GCPluginInfo.NAME, author = GCPluginInfo.AUTHOR, version = GCPluginInfo.VERSION, description = GCPluginInfo.DESC)
@Permissions(@Permission(name = "ghostcraft.all"))
@Commands(@Command(name = "ghostcraft", aliases = {"gc", "ghost", "wun"}, permission = "ghostcraft.all"))
public class GhostcraftPlugin extends JavaPlugin {
    private static GhostcraftPlugin instance;

    public static GhostcraftPlugin instance() {
        if (instance == null) {
            throw new IllegalStateException("GhostCraft is not initialized!");
        }
        return instance;
    }

    private GCCommand commands;
    private ModifierCommand modifierCommand;
    @Getter
    private ModifierStore modifierStore;
    @Getter
    private ParticleAgent particleAgent;

    @Override
    public void onEnable() {
        instance = this;
        this.particleAgent = new ParticleAgent(this);
        commands = new GCCommand();
        this.getCommand("ghostcraft").setExecutor(commands);
        this.modifierStore = new ModifierStore(new File(this.getDataFolder(), "modifiers.yml"));
        this.modifierStore.load();
        this.modifierCommand = new ModifierCommand(modifierStore);
        this.getCommand("modifier").setExecutor(modifierCommand);
    }

    @Override
    public void onDisable() {
        this.modifierStore.save();
    }
}
