package lc.hex.ghostcraft.command;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import lc.hex.ghostcraft.GhostcraftPlugin;
import lc.hex.ghostcraft.effect.LogSpiralProvider;
import lc.hex.ghostcraft.effect.ParticleInstance;
import lc.hex.ghostcraft.game.GCGame;
import lc.hex.ghostcraft.roles.AbilityInvoker;
import lc.hex.ghostcraft.roles.impl.GCPlayer;
import lc.hex.ghostcraft.roles.impl.Ghost;

/**
 * Quick class to handle commands
 */
public class GCCommand implements CommandExecutor {
    /**
     * Constants for the ghost's leap were declared here to make debug easier.
     */
    public static double M1 = 1.2, YP = 0.3, M2 = 1.1;

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        /**
         * This command can be used to make the player a ghost, with
         * any implemented abilities for them becoming active.
         */
        if ("test".equalsIgnoreCase(args[0])) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                GCPlayer gcplayer = new GCPlayer(player);
                gcplayer.setRole(new Ghost(gcplayer));
                AbilityInvoker invoker = new AbilityInvoker();
                Bukkit.getPluginManager().registerEvents(invoker, GhostcraftPlugin.instance());
                Bukkit.getScheduler().runTaskTimer(GhostcraftPlugin.instance(), new AbilityInvoker.CooldownHandler(), 1L, 1L);

                GCGame game = new GCGame(null);
            }
            /**
             * Manipulates the constants for the ghost's leap.
             */
        } else if ("setleap".equalsIgnoreCase(args[0])) {
            M1 = Double.parseDouble(args[1]);
            YP = Double.parseDouble(args[2]);
            M2 = Double.parseDouble(args[3]);
        } else if ("spiral".equalsIgnoreCase(args[0])) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                Location target = player.getTargetBlock((Set<Material>) null, 25).getLocation();
                target.add(0, 1, 0);
                LogSpiralProvider provider = new LogSpiralProvider(4, 0.05, 7);
                Set<Location> locations = new HashSet<>();
                for (Vector vector = provider.next(); vector != null; vector = provider.next()) {
                    locations.add(target.clone().add(vector));
                }
                GhostcraftPlugin.instance().getParticleAgent().queueParticlesRepeating(
                        locations.stream().map(location -> new ParticleInstance(location, 77, 0, 0))
                            .collect(Collectors.toSet()),
                        20 * 15);
            }
        }
        return true;
    }
}
