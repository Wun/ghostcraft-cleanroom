package lc.hex.ghostcraft;

public final class GCPluginInfo {
    public static final String NAME = "GhostCraft";
    public static final String AUTHOR = "Hex";
    public static final String VERSION = "${project.version}";
    public static final String DESC = "GhostCraft minigame plugin.";
}
